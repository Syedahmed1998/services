APIs:

LOGIN(POST): https://attendance-app-service.herokuapp.com/api/auth/login
    
    payload: email and password

SIGN UP(POST): https://attendance-app-service.herokuapp.com/api/auth/sign_up
    payload: 
    company ->{
        first_name:aal
        email:ahmedasad2010@gmail.com
        password:asdasdasd
        country:pakistan
        time_zone:381
        has_flexible_time:false
        total_work_time:300
        work_start_time:0800
        grace_time:15
        working_days[0]:tue
        working_days[1]:mon
        coordinate[0]:21.860966
        coordinate[1]:61.990501
        attendance_type[0]:location_base
        attendance_type[1]:qrcode
        account_type:company
    }

    employee ->{
        email:syed.ahmed@findmyadventure.pk
        password:asdasdasd
        account_type:employee
        first_name:ahmed
        last_name:asad
    }

    OTP will be send automatically on sign up

updateProfile(POST): https://attendance-app-service.herokuapp.com/api/auth/updateProfile
    payload:
    company ->{
        first_name:ahmed
        email:ahmedasad2010@gmail.com
        password:asdasdasd
        country:USA
        time_zone:380
        has_flexible_time:false
        total_work_time:300
        work_start_time:0800
        grace_time:15
        working_days[0]:tue
        working_days[1]:mon
        working_days[2]:wed
        coordinate[0]:21.860966
        coordinate[1]:61.990501
        attendance_type[0]:location_base
        attendance_type[1]:manual
        attendance_type[2]:qrcode
        account_type:company
        user_id:60
        company_id:50
    }
    employee ->{
        email:syed@gmail.com
        account_type:employee
        first_name:syed
        last_name:ahmed asad
        user_id:61
    }

    header: {
                token:eyJhbGciOiJIUzI1NiJ9

                for both employee and company
            }


request Otp(POST): https://attendance-app-service.herokuapp.com/api/auth/requestOtp
        payload: email:syed.ahmed@findmyadventure.pk
verify Account(POST): https://attendance-app-service.herokuapp.com/api/auth/verifyAccount
        payload: {
            email:syed.ahmed@findmyadventure.pk
            code:441540

            header: {
                token:eyJhbGciOiJIUzI1NiJ9
            }
        }

forgot passwor(POST): https://attendance-app-service.herokuapp.com/api/auth/forgotPassword
        payload:
            "email":"ahmedasad2010@gmail.com"
        
            OTP will be send on given email for verification


resetPassword(POST): https://attendance-app-service.herokuapp.com/api/auth/resetPassword
        payload:
            email:ahmedasad2010@gmail.com
            password:asdasd123
        

updatePassword(POST): https://attendance-app-service.herokuapp.com/api/auth/updatePassword
        payload:{
            user_id:61
            password:asdasd123

            header: {
                token:eyJhbGciOiJIUzI1NiJ9
            }
        }
        
time zones(GET): https://attendance-app-service.herokuapp.com/api/common/timezone

companies(GET): https://attendance-app-service.herokuapp.com/api/



