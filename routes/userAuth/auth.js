const validator = require("../../utils/user/validate");
const express = require("express");
const objectUtility = require("../../utils/objectUtility");
const authenticate = require("../../utils/authUtility");
const userDB = require("../../database/user");

const user = userDB.getInstance();
const router = express.Router();
router.use(express.json());

router.post("/sign_up", async (req, res) => {
  const { error } = validator.userSchema().validate(req.body);
  if (error) return res.status(409).json({ message: error.message });
  try {
    const OTPCode = Math.floor(100000 + Math.random() * 900000);
    await user.checkIfUserExist(req.body.email);
    if (req.body.account_type === "company") {
      const { error } = validator.companySchema().validate(req.body);
      if (error) return res.status(409).json({ message: error.message });
      if (JSON.parse(req.body.has_flexible_time) == false) {
        if (!req.body.work_start_time)
          return res
            .status(409)
            .json({ message: "please provide day start time" });
        if (!req.body.grace_time)
          return res.status(409).json({ message: "please provide grace time" });
        if (req.body.attendance_type.includes("location_base")) {
          if (!req.body.coordinate)
            return res.status(409).json({ message: "please provide location" });

          if (req.body.coordinate.length < 2 || req.body.coordinate.length > 2)
            return res
              .status(409)
              .json({ message: "please provide proper location" });
        }
      }

      const workingDayId = await user.insertWorkingDays(req.body.working_days);
      const attendanceId = await user.insertAttendanceType(
        req.body.attendance_type
      );
      const data = await user.createUser(req.body, OTPCode, "company");

      const company = objectUtility.companyObject(
        data.insertId,
        req.body.first_name,
        req.body.country,
        workingDayId.insertId,
        attendanceId.insertId,
        req.body.has_flexible_time,
        req.body.time_zone,
        req.body.work_start_time == null ? null : req.body.work_start_time,
        req.body.grace_time == null ? null : req.body.grace_time,
        req.body.total_work_time,
        req.body.coordinate != null ? req.body.coordinate[0] : -1,
        req.body.coordinate != null ? req.body.coordinate[1] : -1
      );

      await user.createCompany(company);
      const successData = await user.getCompanyData(req.body.email);

      const response = user.makeCompanyObject(successData);
      res.status(200).json({
        body: {
          response,
          token: user.generateAccessToken(JSON.stringify(response))
        }
      });
    } else if (req.body.account_type === "employee") {
      const data = await user.createUser(req.body, OTPCode, "employee");
      await user.createEmployee(data.insertId);
      response = await user.getEmployeeData(req.body.email);
      res.status(200).json({
        body: user.getEmployeeObject(response)
      });
    } else {
      return res.status(403).json({ message: "please specify account type!" });
    }
    await user.sendOtpCode(req.body.email, OTPCode);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post("/login", async (req, res) => {
  const { error } = validator.authenticateUser().validate(req.body);
  if (error) return res.status(409).json({ message: error.message });
  try {
    const currentUser = await user.authenticateUser(req.body);

    if (currentUser.account_type == "company") {
      const userData = await user.getCompanyData(currentUser.email);
      const response = user.makeCompanyObject(userData);
      res.status(200).json({
        body: {
          response,
          token: user.generateAccessToken(JSON.stringify(response))
        }
      });
    } else if (currentUser.account_type === "employee") {
      const userData = await user.getEmployeeData(currentUser.email);
      res.status(200).json({
        body: {
          user_id: userData.user_id,
          email: userData.email,
          is_verified: userData.is_verified == 0 ? false : true,
          user_name: userData.user_name,
          account_type: userData.account_type,
          employee_id: userData.employee_id,
          profile_image: userData.profile_image,
          token: user.generateAccessToken(JSON.stringify(userData))
        }
      });
    } else {
      return res.status(403).json({ message: "please specify account type!" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.put(
  "/updateProfile",
  authenticate.authenticateToken,
  async (req, res) => {
    const { error } = validator.userUpdateSchema().validate(req.body);
    if (error) return res.status(409).json({ message: error.message });

    if (req.body.account_type === "company") {
      let { error } = validator.companySchema().validate(req.body);
      if (error) return res.status(409).json({ message: error.message });

      const validation = validator.updateCompany().validate(req.body);
      if (validation.error)
        return res.status(409).json({ message: validation.error.message });
      if (JSON.parse(req.body.has_flexible_time) == false) {
        if (!req.body.work_start_time)
          return res
            .status(409)
            .json({ message: "please provide day start time" });
        if (!req.body.grace_time)
          return res.status(409).json({ message: "please provide grace time" });
        if (req.body.attendance_type.includes("location_based")) {
          if (!req.body.coordinate)
            return res.status(409).json({ message: "please provide location" });

          if (req.body.coordinate.length < 2 || req.body.coordinate.length > 2)
            return res
              .status(409)
              .json({ message: "please provide proper location" });
        }
      }

      if (req.user.data.user_id != req.body.user_id || !req.user.data.user_id) {
        res.status(409).json({ message: "Invalid token / user id." });
      }
      const attendanceAndWorkingDayId = await user.getAttendanceType(
        req.body.company_id
      );
      await user.updateWorkingDays(
        req.body.working_days,
        attendanceAndWorkingDayId.working_days
      );

      await user.updateAttendanceType(
        req.body.attendance_type,
        attendanceAndWorkingDayId.attendance_type
      );

      await user.updateUser(req.body);
      const company = objectUtility.companyObject(
        req.body.user_id,
        req.body.first_name,
        req.body.country,
        attendanceAndWorkingDayId.working_days,
        attendanceAndWorkingDayId.attendance_type,
        req.body.has_flexible_time,
        req.body.time_zone,
        req.body.work_start_time == null ? null : req.body.work_start_time,
        req.body.grace_time == null ? null : req.body.grace_time,
        req.body.total_work_time,
        req.body.coordinate[0],
        req.body.coordinate[1]
      );
      await user.updateCompany(company);
      const successData = await user.getCompanyData(req.body.email);
      const response = user.makeCompanyObject(successData);
      res.status(200).json({
        body: {
          response,
          token: user.generateAccessToken(JSON.stringify(response))
        }
      });
    } else if (req.body.account_type === "employee") {
      const validation = validator.updateUser().validate(req.body);
      if (validation.error)
        return res.status(409).json({ message: validation.error.message });
      if (req.user.user_id != req.body.user_id || !req.user.user_id) {
        res.status(409).json({ message: "Invalid token / user id." });
      }
      await user.updateUser(req.body);

      response = await user.getEmployeeData(req.body.email);
      res.status(200).json({
        body: user.getEmployeeObject(response)
      });
    } else {
      return res.status(403).json({ message: "please specify account type!" });
    }
  }
);

router.post("/verifyAccount", async (req, res) => {
  try {
    const { error } = validator.otpVerification().validate(req.body);
    if (error) return res.status(409).json({ message: error.message });
    const data = await user.verifyOTPCode(req.body.email, req.body.code);
    if (data.email == req.body.email)
      await user.updateAccountVerification(req.body.email, req.body.user_id);
    else return res.status(409).json({ message: "invalid email" });
    res.status(200).json({ message: "Account has verified" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post("/requestOtp", async (req, res) => {
  try {
    const { error } = validator.requestOtp().validate(req.body);
    if (error) return res.status(409).json({ message: error.message });
    const OTPCode = Math.floor(100000 + Math.random() * 900000);
    await user.checkIfUserExistInDB(req.body.email);
    await user.setUsercode(req.body.email, OTPCode);
    await user.sendOtpCode(req.body.email, OTPCode);
    res
      .status(200)
      .json({ message: "otp has been sent on you email address." });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post(
  "/updatePassword",
  authenticate.authenticateToken,
  async (req, res) => {
    try {
      const { error } = validator.verifyPassword().validate(req.body);
      if (error) return res.status(409).json({ message: error.message });
      await user.checkIfUserExistInDBWithId(req.body.user_id);
      if (req.user.data != null) {
        if (req.user.data.user_id != req.body.user_id)
          return res.status(409).json({ message: "invalid token / user id" });
      } else if (req.user != null) {
        if (req.user.user_id != req.body.user_id)
          return res.status(409).json({ message: "invalid token / user id" });
      }
      user.updatePassword(req.body.user_id, req.body.password);
      res.status(200).json({ message: "password has been updated." });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

router.post("/resetPassword", async (req, res) => {
  try {
    const { error } = validator.resetPassword().validate(req.body);
    if (error) return res.status(409).json({ message: error.message });
    await user.checkIfUserExistInDB(req.body.email);

    await user.resetPassword(req.body.email, req.body.password);
    res.status(200).json({ message: "password has been updated." });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post("/forgotPassword", async (req, res) => {
  try {
    const { error } = validator.requestOtp().validate(req.body);
    if (error) return res.status(409).json({ message: error.message });
    const otpCode = Math.floor(100000 + Math.random() * 900000);
    await user.checkIfUserExistInDB(req.body.email);
    await user.forgotPassowrd(req.body.email, otpCode);
    await user.sendOtpCode(req.body.email, otpCode);
    res.status(200).json({ message: "OTP has been sent on you email." });
  } catch (err) {
    res.status(err.status).json({ message: err.message });
  }
});

module.exports = router;
