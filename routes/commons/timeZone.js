const express = require("express");
const dbConnection = require("../../database/config");

const router = express.Router();
router.use(express.json());

router.get("/timezone", async (req, res) => {
  try {
    const timeZones = await getAllTimeZones();
    res.status(200).json({ timeZones });
  } catch (err) {
    res.status(err.status).json({ message: err.message });
  }
});

router.get("/companies", async (req, res) => {
  try {
    const companies = await getAllCompanies();
    res.status(200).json({ companies });
  } catch (err) {
    res.status(err.status).json({ message: err.message });
  }
});

function getAllCompanies() {
  return new Promise((resolve, reject) => {
    dbConnection.connection.query(
      `select
    users.id as user_id,users.email,users.user_name,
    company.id as company_id,company.company_code,company.country,company.latitude,company.longitude
from
    users
        join company `,
      (err, res) => {
        if (err) reject({ status: 500, message: err.message });
        else if (res.length) resolve(res);
        else reject({ status: 404, message: "No company found" });
      }
    );
  });
}

function getAllTimeZones(id) {
  return new Promise((resolve, reject) => {
    dbConnection.connection.query(`select * from time_zone`, (err, res) => {
      if (err) reject({ status: 500, message: err.message });
      else if (res.length) resolve(res);
      else reject({ status: 404, message: "No time zone found" });
    });
  });
}

module.exports = router;
