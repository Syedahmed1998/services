const database = require("../database/config");
const mailer = require("../service/mailer");
const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");
dotenv.config();

let dbConnection = null;
let instance = null;
const connection = database.connection;

class UserDB {
  static getInstance() {
    dbConnection = connection.connect;
    return instance ? instance : new UserDB();
  }

  checkIfUserExist(email) {
    return new Promise((resolve, reject) => {
      connection.query(
        `select email from users where email='${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else if (!res.length) resolve(res);
          else reject({ status: 409, message: "This email is already in use" });
        }
      );
    });
  }

  insertWorkingDays(days) {
    return new Promise((resolve, reject) => {
      connection.query(
        `insert into working_days (mon,tue,wed,thu,fri,sat,sun) values('${
          days.includes("mon") ? 1 : 0
        }','${days.includes("tue") ? 1 : 0}','${
          days.includes("wed") ? 1 : 0
        }','${days.includes("thu") ? 1 : 0}','${
          days.includes("fri") ? 1 : 0
        }','${days.includes("sat") ? 1 : 0}','${
          days.includes("sun") ? 1 : 0
        }')`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  updateWorkingDays(days, id) {
    return new Promise((resolve, reject) => {
      connection.query(
        `UPDATE working_days SET mon = '${days.includes("mon") ? 1 : 0}',
        tue='${days.includes("tue") ? 1 : 0}',wed='${
          days.includes("wed") ? 1 : 0
        }',
        thu='${days.includes("thu") ? 1 : 0}',fri='${
          days.includes("fri") ? 1 : 0
        }',
        sat='${days.includes("sat") ? 1 : 0}',sun='${
          days.includes("sun") ? 1 : 0
        }'
        WHERE id='${id}' `,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  insertAttendanceType(type) {
    return new Promise((resolve, reject) => {
      connection.query(
        `insert into attendance_type (location_base,manual,qr_code) values('${
          type.includes("location_base") ? 1 : 0
        }','${type.includes("manual") ? 1 : 0}','${
          type.includes("qrcode") ? 1 : 0
        }')`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  updateAttendanceType(type, id) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update attendance_type set location_base = '${
          type.includes("location_base") ? 1 : 0
        }',manual = '${type.includes("manual") ? 1 : 0}',qr_code='${
          type.includes("qrcode") ? 1 : 0
        }' where id = '${id}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  getAttendanceType(id) {
    return new Promise((resolve, reject) => {
      connection.query(
        `select attendance_type,working_days from company where id = '${id}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res[0]);
        }
      );
    });
  }

  createUser(user, verification_code, account_type) {
    return new Promise((resolve, reject) => {
      connection.query(
        `insert into users(email,user_name,password,is_verified,verification_code,account_type) values('${
          user.email
        }','${user.first_name} ${
          user.last_name == null ? "" : user.last_name
        }','${user.password}','${0}','${verification_code}','${account_type}')`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  updateUser(user) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set email = '${user.email}',user_name='${
          user.first_name
        }${user.last_name == null ? "" : " " + user.last_name}' where id = '${
          user.user_id
        }'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  createCompany(company) {
    return new Promise((resolve, reject) => {
      connection.query(
        `insert into company (user_id, company_code, country,time_zone, attendance_type,
          working_days, has_flexible_time, total_work_time, work_start_time, grace_time,latitude,longitude) values('${
            company.user_id
          }','${company.company_code}','${company.country}','${
          company.time_zone
        }','${company.attendance_id}','${company.working_day_id}',
        '${company.has_flexible_time == false ? 0 : 1}','${
          company.total_work_time
        }','${company.work_start_time}','${company.grace_time}','${
          company.latitude == null ? null : company.latitude
        }','${company.longitude == null ? null : company.longitude}')`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  updateCompany(company) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update company set time_zone = '${company.time_zone}',country = '${
          company.country
        }', has_flexible_time='${
          company.has_flexible_time == false ? 0 : 1
        }', total_work_time= '${company.work_start_time}', work_start_time='${
          company.total_work_time
        }', grace_time= '${company.grace_time}',latitude= '${
          company.latitude == null ? null : company.latitude
        }', longitude='${
          company.longitude == null ? null : company.longitude
        }' where user_id = ${company.user_id}`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  getCompanyData(email) {
    return new Promise((resolve, reject) => {
      connection.query(this.companyDataQuery(email), (err, res) => {
        if (err) reject({ status: 500, message: err.message });
        if (!res) {
          reject({ status: 404, message: "No User found with this email." });
        } else {
          resolve(res[0]);
        }
      });
    });
  }

  updateProfileImage(id, imageUrl) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set profile_image='${imageUrl}' where id = '${id}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          if (!res)
            reject({ status: 404, message: "No User found with this email." });
          else {
            resolve(res[0]);
          }
        }
      );
    });
  }

  getEmployeeData(email) {
    return new Promise((resolve, reject) => {
      connection.query(this.employeeDataQuery(email), (err, res) => {
        if (err) reject({ status: 500, message: err.message });
        if (!res)
          reject({ status: 404, message: "No User found with this email." });
        else {
          resolve(res[0]);
        }
      });
    });
  }

  createEmployee(employeeId) {
    return new Promise((resolve, reject) => {
      connection.query(
        `insert into employee (user_id) values('${employeeId}')`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  getEmployeeObject(response) {
    return {
      user_id: response.user_id,
      email: response.email,
      is_verified: response.is_verified == 0 ? false : true,
      user_name: response.user_name,
      account_type: response.account_type,
      employee_id: response.employee_id,
      profile_image: response.profile_image,
      token: this.generateAccessToken(JSON.stringify(response))
    };
  }

  makeCompanyObject(data) {
    return {
      working_days: {
        mon: data.mon == 1 ? true : false,
        tue: data.tue == 1 ? true : false,
        wed: data.wed == 1 ? true : false,
        thu: data.thu == 1 ? true : false,
        fri: data.fri == 1 ? true : false,
        sat: data.sat == 1 ? true : false,
        sun: data.sub == 1 ? true : false
      },
      attendance_type: {
        location_base: data.location_base == 1 ? true : false,
        qr_code: data.qr_code == 1 ? true : false,
        manual: data.manual == 1 ? true : false
      },
      data: {
        user_id: data.user_id,
        email: data.email,
        is_verified: data.is_verified == 0 ? false : true,
        user_name: data.user_name,
        company_id: data.company_id,
        company_code: data.company_code,
        country: data.country,
        has_flexible_time: data.has_flexible_time == 0 ? false : true,
        total_work_time: data.total_work_time,
        work_start_time: data.work_start_time,
        grace_time: data.grace_time,
        latitude: data.latitude,
        longitude: data.longitude,
        zone: data.zone,
        account_type: data.account_type,
        profile_image: data.profile_image
      }
    };
  }

  employeeDataQuery(email) {
    return `select
      users.id as user_id,users.email,users.is_verified,users.user_name,users.account_type,users.profile_image,
      employee.id as employee_id 
      from users 
      join employee on users.email = '${email}' and employee.user_id = users.id`;
  }

  companyDataQuery(email) {
    return `select
      users.id as user_id,users.email,users.is_verified,users.user_name,users.account_type,profile_image,
      company.id as company_id,company.company_code,company.country,company.has_flexible_time,company.total_work_time,company.work_start_time,company.grace_time,company.latitude,company.longitude,
      time_zone.zone,
      attendance_type.location_base,attendance_type.qr_code,attendance_type.manual,
      working_days.mon,working_days.tue,working_days.wed,working_days.thu,working_days.fri,working_days.sat,working_days.sun
  from
      users
          join company on users.email='${email}' and company.user_id = users.id
          join time_zone on time_zone.id = company.time_zone
          join attendance_type on attendance_type.id = company.attendance_type
          join working_days on working_days.id = company.working_days`;
  }

  authenticateUser(user) {
    return new Promise((resolve, reject) => {
      connection.query(
        `select * from users where email='${user.email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          if (!res.length)
            reject({ status: 404, message: "No User found with this email." });
          else {
            if (res[0].password == user.password) resolve(res[0]);
            else reject({ status: 409, message: "Invalid email or password" });
          }
        }
      );
    });
  }

  checkIfUserExistInDB(email) {
    return new Promise((resolve, reject) => {
      connection.query(
        `select email from users where email='${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else if (res.length == 0) {
            reject({ status: 409, message: "no user found with this email." });
          } else resolve(res);
        }
      );
    });
  }

  checkIfUserExistInDBWithId(id) {
    return new Promise((resolve, reject) => {
      connection.query(`select * from users where id='${id}'`, (err, res) => {
        if (err) reject({ status: 500, message: err.message });
        else if (!res.length) {
          reject({ status: 409, message: "no user found." });
        } else resolve(res);
      });
    });
  }

  updateAccountVerification(email) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set is_verified = '${1}' where email = '${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve({ status: 200, message: "Account has verified." });
        }
      );
    });
  }

  verifyOTPCode(email, verification_code) {
    return new Promise((resolve, reject) => {
      connection.query(
        `select * from users where email='${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else if (!res.length)
            reject({ status: 404, message: "No User found with this email." });
          else if (res[0].verification_code == verification_code)
            resolve(res[0]);
          else reject({ status: 409, message: "Invalid code." });
        }
      );
    });
  }

  setUsercode(email, code) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set verification_code='${code}' where email = '${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          if (res.length)
            reject({ status: 404, message: "No User found with this email." });
          else {
            resolve(res[0]);
          }
        }
      );
    });
  }

  updatePassword(id, password) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set password='${password}' where id = '${id}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else {
            resolve(res);
          }
        }
      );
    });
  }

  resetPassword(email, password) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set password='${password}' where email = '${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else {
            resolve(res);
          }
        }
      );
    });
  }
  forgotPassowrd(email, otpCode) {
    return new Promise((resolve, reject) => {
      connection.query(
        `update users set verification_code='${otpCode}', is_verified='${0}' where email = '${email}'`,
        (err, res) => {
          if (err) reject({ status: 500, message: err.message });
          else resolve(res);
        }
      );
    });
  }

  sendOtpCode(email, code) {
    mailer.transporter.sendMail({
      from: "aalsolution21@gmail.com", // sender address
      to: email, // list of receivers
      subject: "OTP code", // Subject line
      text: `following OTP for your account verification. \n 
      code: ${code}` // plain text body
    });
  }

  generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
  }
}

module.exports = UserDB;
