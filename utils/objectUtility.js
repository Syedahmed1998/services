company = function company(
  id,
  name,
  country,
  workingDayId,
  attendanceId,
  has_flexible_time,
  time_zone,
  work_start_time,
  grace_time,
  total_work_time,
  latitude,
  longitude
) {
  return {
    user_id: id,
    company_code: `${name}${id}`,
    country: country,
    has_flexible_time: has_flexible_time,
    working_day_id: workingDayId,
    attendance_id: attendanceId,
    time_zone: time_zone,
    work_start_time: work_start_time == null ? -1 : work_start_time,
    grace_time: grace_time == null ? -1 : grace_time,
    total_work_time: total_work_time,
    latitude: latitude,
    longitude: longitude
  };
};

module.exports = {
  companyObject: company
};
