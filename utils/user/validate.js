const Joi = require("joi");

userSchema = function() {
  return Joi.object({
    account_type: Joi.string()
      .valid("employee", "company")
      .alphanum()
      .required()
      .error(Error("Please specify account type")),
    first_name: Joi.string()
      .required()
      .error(Error("Please provide User Name")),
    last_name: Joi.string()
      .alphanum()
      .error(Error("Please provide last name")),
    password: Joi.string()
      .alphanum()
      .min(8)
      .required()
      .error(Error("Please provide password of alteast 8 characters")),
    email: Joi.string()
      .email()
      .required()
      .error(Error("Please Provide valid email address"))
  }).unknown();
};

userUpdateSchema = function() {
  return Joi.object({
    account_type: Joi.string()
      .valid("employee", "company")
      .required()
      .error(Error("Please specify account type")),
    first_name: Joi.string()
      .required()
      .error(Error("Please provide User Name")),
    last_name: Joi.string().error(Error("Please provide last name")),
    email: Joi.string()
      .email()
      .required()
      .error(Error("Please Provide valid email address"))
  }).unknown();
};

companySchema = function() {
  return Joi.object({
    country: Joi.string()
      .alphanum()
      .required()
      .error(Error("Please provide country name")),
    has_flexible_time: Joi.boolean()
      .required()
      .error(Error("Please let us know about time flexibility")),
    attendance_type: Joi.array()
      .items(Joi.string().valid("location_base", "qrcode", "manual"))
      .required()
      .error(Error("please provide valid attendance type")),
    total_work_time: Joi.number()
      .required()
      .error(Error("please provide total working hours a day")),
    time_zone: Joi.number()
      .required()
      .error(Error("please provide time zone")),
    working_days: Joi.array()
      .items(
        Joi.string().valid("mon", "tue", "wed", "thu", "fri", "sat", "sun")
      )
      .required()
      .error(Error("please provide valid working day(s)"))
  }).unknown();
};

updateCompany = function() {
  return Joi.object({
    company_id: Joi.number()
      .required()
      .error(Error("Please provide company id")),
    user_id: Joi.number()
      .required()
      .error(Error("Please provide user id"))
  }).unknown();
};

updateUser = function() {
  return Joi.object({
    user_id: Joi.number()
      .required()
      .error(Error("Please provide user id"))
  }).unknown();
};

authenticateUser = function() {
  return Joi.object({
    email: Joi.string()
      .email()
      .required()
      .error(Error("Please Provide valid email address")),
    password: Joi.string()
      .required()
      .error(Error("Please provide password"))
  });
};

requestOtp = function() {
  return Joi.object({
    email: Joi.string()
      .email()
      .required()
      .error(Error("Please Provide valid email address"))
  });
};

verifyPassword = function() {
  return Joi.object({
    user_id: Joi.number()
      .required()
      .error(Error("Please Provide valid user id")),
    password: Joi.string()
      .alphanum()
      .min(8)
      .required()
      .error(Error("Please provide password of alteast 8 characters"))
  });
};

resetPassword = function() {
  return Joi.object({
    email: Joi.string()
      .email()
      .required()
      .error(Error("Please Provide valid email address")),
    password: Joi.string()
      .alphanum()
      .min(8)
      .required()
      .error(Error("Please provide password of alteast 8 characters"))
  });
};

otpVerification = function() {
  return Joi.object({
    email: Joi.string()
      .email()
      .required()
      .error(Error("Please Provide valid email address")),
    code: Joi.string()
      .length(6)
      .required()
      .error(Error("Please provide proper code"))
  });
};

module.exports = {
  userSchema: userSchema,
  companySchema: companySchema,
  authenticateUser: authenticateUser,
  otpVerification: otpVerification,
  requestOtp: requestOtp,
  verifyPassword: verifyPassword,
  resetPassword: resetPassword,
  userUpdateSchema: userUpdateSchema,
  updateCompany: updateCompany,
  updateUser: updateUser
};
