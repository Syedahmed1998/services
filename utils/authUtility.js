const jwt = require("jsonwebtoken");

module.exports = {
  authenticateToken(req, res, next) {
    let authToken = req.headers["token"];
    if (authToken == null)
      return res.status(401).json({ message: "Please provide access token" });
    jwt.verify(authToken, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      if (err) return res.json({ err: err.message });
      req.user = user;
      next();
    });
  }
};
