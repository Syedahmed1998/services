const express = require("express");
const authenticate = require("./utils/authUtility");
const auth = require("./routes/userAuth/auth");
const common = require("./routes/commons/timeZone");
const upload = require("./service/uploads");
const mailer = require("./service/mailer");
const zoneUpload = require("./service/uploadFiles");
const fs = require("fs");
const morgan = require("morgan");
const startUpDebugger = require("debug")("app:startup");
const dbDebugger = require("debug")("app:db");

// startUpDebugger();
// dbDebugger();
// const home = require("./routes/home");
const app = express();

// app.use(morgan("tiny"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/docs", express.static("public"));
app.use("/api/auth", auth);
app.use("/api/common", common);
app.use("/api/uploads", upload);

// app.use("/", home);
// let stream = fs.createReadStream("zones.csv");

// stream.pipe(zoneUpload.csvStream);

app.get("/", (req, res) => {
  res.send("Welcome to AAL SOlution ");
});

// app.get("/email", (req, res) => {
//   mailer.transporter.sendMail({
//     from: "ahmedasad2010@gmail.com", // sender address
//     to: "ahmedasad2010@gmail.com", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>" // html body
//   });
// });

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
