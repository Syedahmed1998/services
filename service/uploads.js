const express = require("express");
const multer = require("multer");
const path = require("path");
const helper = require("../helper/helper");
const AWS = require("aws-sdk");
const authenticate = require("../utils/authUtility");
const dotenv = require("dotenv");
const { v4: uuidv4 } = require("uuid");
const userDB = require("../database/user");

const user = userDB.getInstance();
dotenv.config();
const router = express.Router();
router.use(express.json());

const s3BucketStorage = new AWS.S3({
  accessKeyId: process.env.AWS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET
});

const storage = multer.memoryStorage({
  destination: function(req, file, callBack) {
    cb(null, "/images");
  }
});

let upload = multer({
  storage: storage,
  fileFilter: helper.imageFilter
}).single("file");

router.post(
  "/profile_image",
  authenticate.authenticateToken,
  async (req, res) => {
    try {
      upload(req, res, err => {
        if (req.user.user_id != req.body.user_id || !req.user.user_id) {
          return res.status(409).json({ message: "Invalid token / user id." });
        }
        if (!req.file)
          return res.status(409).json({ message: `file is required.` });

        let fileInfo = req.file;
        let fileName = fileInfo.originalname.split(".");
        fileType = fileName[fileName.length - 1];
        if (!req.file) {
          return res.status(409).json({ message: "please provide image" });
        } else if (err instanceof multer.MulterError) {
          return res.status(409).send(err);
        } else if (err) {
          return res.status(409).send(err);
        }

        const params = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Key: `${uuidv4()}.${fileType}`,
          Body: fileInfo.buffer
        };

        s3BucketStorage.upload(params, (error, data) => {
          if (error)
            return res.status(500).json({ message: `${error.message}` });
          user.updateProfileImage(req.user.user_id, data.Location);
          res.status(200).json({ url: data.Location });
        });
      });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

module.exports = router;
