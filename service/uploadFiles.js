const dbConnection = require("../database/config");
const fastcsv = require("fast-csv");

let fileData = [];
module.exports = {
  csvStream: (csvStream = fastcsv
    .parse()
    .on("data", data => {
      fileData.push(data);
    })
    .on("end", () => {
      fileData.shift();
      dbConnection.connection.query(
        "insert into time_zone(country,zone) values ?",
        [fileData],
        (err, res) => {
          if (err) console.log(err.message);
          else console.log(res);
        }
      );
    }))
};
